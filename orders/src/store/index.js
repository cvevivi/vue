import { createStore } from "vuex";
const store = createStore({
    state (){
        return {
            apiUrl : "http://localhost:3001",
            student_id: 122, 
            orders : [],
            deliveries : [],
            isDarkTheme : false
        }
    },
    actions: {
        async getOrders(ctx){
            let id = ctx.state.student_id;
            const res = await fetch (`${ctx.state.apiUrl}/orders`, {
            method : "GET",
            headers : {
              "Content-Type" : "appication/json;charset=utf-8"
            }})
            let result = await res.json();
            this.commit("setOrders", result)
            return true;
        },
        async getDeliveries(ctx){
            let id = ctx.state.student_id;
            const res = await fetch (`${ctx.state.apiUrl}/deliveries`, {
                method : "GET",
                headers : {
                "Content-Type" : "appication/json;charset=utf-8"
                }})
            let result = await res.json();
            this.commit("setDeliveries", result)
            return true;
        },
        async delivery(ctx, order_id){
            let student_id = ctx.state.student_id;
            const res = await fetch (`${ctx.state.apiUrl}/orders/${order_id}/delivery`, {
                method : "POST",
                headers : {
                    "Content-Type" : "appication/json;charset=utf-8"
                }})
            let result = await res.json();
            if(result){
                ctx.dispatch("getDeliveries").then(data => {
                    return true;
                });
            }
            else{
                return false;
            }
        },
        async removeDelivery(ctx, delivery_id){
            const res = await fetch (`${ctx.state.apiUrl}/deliveries/${delivery_id}`, {
                method : "DELETE",
                headers : {
                    "Content-Type" : "appication/json;charset=utf-8"
                }})
            let result = await res.json();
            if(result){
                ctx.dispatch("getDeliveries").then(data => {
                    return true;
                });
            }
            else{
                return false;
            }
        },
        async removeOrder(ctx, order_id){
            let student_id = ctx.state.student_id;
            const res = await fetch (`${ctx.state.apiUrl}/orders/${order_id}`, {
                method : "DELETE",
                headers : {
                    "Content-Type" : "appication/json;charset=utf-8"
                }})
            let result = await res.json();
            if(result){
                ctx.dispatch("getOrders").then(data => {
                    return true;
                });
            }
            else{
                return false;
            }
        },
    },
    mutations: {
        setOrders(ctx, orders){
            console.log(orders);
            ctx.orders = orders;
        },
        setDeliveries(ctx, deliveries){
            console.log(deliveries);
            ctx.deliveries = deliveries;
        },
        setTheme(ctx){
            ctx.isDarkTheme = !ctx.isDarkTheme;
        }
    },
    getters: {
        getOrders(ctx){
            return ctx.orders;
        },
        getDeliveries(ctx){
            return ctx.deliveries;
        }
    }
})
export default store